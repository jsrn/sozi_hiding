#!/usr/bin/python3
# Merges a Sozi presentation with a small script for hiding certain objects
# whenever the frame is not as specified.
# Copyright Johan S. R. Nielsen
# Released under the GPL v3 or later.
import sys, io, subprocess, re
import lxml
from lxml import etree

#TODO: Improvements
# - Hiding doesn't work on the first frame or if one goes directly to a frame using URL.
# - One often wish to specify "visible on this frame and also during next transition". This
#   now requires explicit naming of the next frame, and could be made nicer.
# - Instead of going through all frames each time, hold the last frame number
#   visited and compare with the new one. Use fast lookup methods to determine
#   which objects are relevant to examine and set the state of these.
# - The above should also allow several intervals for the same object (if
#   anyone should want that)

#Hiding-list-file type
if not len(sys.argv) == 3:
    print("""Usage:
   sozi_hiding.py <sozi-presentation.svg> <hiding-list-file>

Each line of hiding-list-file should be of the form (no leading spaces)
   <objid> <frame-spec>
where objid is the SVG id of the object that should be hidden, and frame-spec
is a frame specification of the form
   <frame-no>        OR       [from-frame]-[to-frame]
Framenumbers are integers and frames start numbering from 1. In the first case,
the object will be visible only on the given frame. In the second case it will
be visible in the interval specified, both inclusive. from-frame or to-frame
might be left unspecified in which case it is taken as 0, respectively 999999.
""")
    sys.exit(1)

hide_script = etree.Element("script")
hide_script_body = """
    sozi.events.listen("sozi.player.framechange", function (index) {
    for (var i = 0; i < objList.length; i++) {
    document.getElementById(objList[i].id).style.visibility =
        (objList[i].from <= index && objList[i].to >= index)
            ? "visible" : "hidden";
    }
    });
    """

pat_obj = r"([^\s]+)"
pat_frame_name_simple = r"([^\s\"-]*)"
pat_frame_name_comp = r"(\"([^\"\n]*)\")"
pat_frame = r"(%s|%s)" % (pat_frame_name_simple, pat_frame_name_comp)
pat_snd_frame = r"(-\s*"+ pat_frame +r"?)?"
pat = r"\s*".join([ pat_obj, pat_frame, pat_snd_frame, r"\n?\Z"])
grp_obj, grp_frame_from, grp_frame_dash, grp_frame_to = 1, 2, 6, 7 
pat_prog = re.compile(pat)

def obj_str_from_line(line, frame_map, id_set):
    m = pat_prog.match(line)
    if not m:
        raise Exception("Frame spec does not match syntax:\n\t" + line)
    objid = m.group(grp_obj)
    if not objid in id_set:
        raise Exception("Object id is not in the SVG: " + objid)
    def get_frame_id(grp_off):
        def lookup_name(name):
            if not name in frame_map:
                raise Exception("Unknown frame title: '"+ name +"'")
            return frame_map[name]
        if m.group(grp_off+1):
            #Matched digits or single-word name
            word = m.group(grp_off+1)
            try:
                return int(word)
            except ValueError:
                return lookup_name(word)
        elif m.group(grp_off+2):
            #Matched multi-word name
            return lookup_name(m.group(grp_off+3))
        else:
            #Huh?
            raise Exception("Somethings wrong: " + line)
    if m.group(grp_frame_from):
        frame_from = get_frame_id(grp_frame_from)
    else:
        frame_from = 0
    if m.group(grp_frame_dash):
        if m.group(grp_frame_to) == '':
            frame_to = 99999
        else:
            frame_to = get_frame_id(grp_frame_to)
    else:
        frame_to = frame_from
    #Correct for 0-indexed in JavaScript
    frame_from -= 1
    frame_to -= 1
    return "\t{id: \"%s\", from: %s, to: %s}," % (objid, frame_from, frame_to)

def objList_assign(lines, frame_map, id_set):
    nlines = []
    for l in lines:
        ls = l.strip()
        if ls != "":
            nlines.append(ls)
    return "\n".join(["var objList = ["]+[ obj_str_from_line(l, frame_map, id_set) for l in nlines]+["];"]);

svg_file = sys.argv[1]

parser = etree.XMLParser(huge_tree=True)
svg_tree = etree.parse(svg_file, parser=parser)

#Find frame name/id dictionary
ns = "{http://sozi.baierouge.fr}"
frame_map = dict()
frames = svg_tree.findall(ns+"frame")
for frame in frames:
    frame_map[frame.attrib[ns+"title"]] = int(frame.attrib[ns+"sequence"])

#Find set of all ids
objids = svg_tree.xpath('//@id')
id_set = set(objids)

hides_handle = open(sys.argv[2],'r')
hide_script = etree.Element("script")
hide_script.text = etree.CDATA(objList_assign(hides_handle, frame_map, id_set) \
                                   + hide_script_body)
svg_tree.getroot().append(hide_script)

new_file = svg_file[:-4] + "_h.svg"
svg_tree.write(new_file, pretty_print='true')

print("Done")
