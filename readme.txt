Adds functionality of appearing/disappearing objects during a Sozi presentation.

To use, create a hide-file which line-by-line specifies objects which should
appear/disappear at certain frames, and then merge this with the SVG Sozi
presentation using sozi.hiding.py:

./sozi_hiding.py drawing.svg drawing.hide

This will create a new file called "drawing_h.svg" which is the drawing.svg
presentation with the appearing/disappearing action included.

Objects are referenced by ID (in Inkscape, this can be set in Object
Properties). Frames can be referenced by their Sozi name or number. Each line of
a hide file specifies the visibility of one object (so use groups for many
objects at the same time). You can specify which frames an object should be
visible at in four ways:

1) A single frame:
  obj1 "Frame1"
  
2) From a certain frame and to a certain one, both inclusive:
  obj1 "FrameFrom" - "FrameTo"

3) From a certain frame and onwards:
  obj1 "FrameFrom" -

4) From the beginning until and including a certain frame:
  obj1  - "FrameTo"

A frame name can be given with or without the quotation marks. If given without
the quotation marks, hyphens '-' and spaces are not allowed (as that would
conflict with the rest of the syntax).

An example is included and demonstrates the simple syntax of the hide files. As
usually for a Sozi-presentation, you can press F on any frame to zoom out and
see the entire canvas, for verifying that the objects are indeed visible or
hidden.

This code is released as free software under the GNU Public License v3.

Johan S. R. Nielsen
jsrn.dk
